package com.cajero.modelos;

import com.cajero.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 *
 * @author Tefa
 */


public class Estudiante {
    
    // Atributos (int,float,double,String,boolean)
    private int id;
    private String nombre;
    private String correo;
    private int edad;
    
    
    // Constructor

    public Estudiante(int id, String nombre, String correo, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.edad = edad;
    }
    
    public Estudiante(){
        
    }
    
    //Metodos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public float getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD - R 
        String  sql = "SELECT id,nombre,correo,edad FROM estudiantes WHERE id = "+this.getId()+";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);// recibimos la tabla de la consulta que acabamos de hacer (tabla estudiante)
        while (rs.next()) // Recorremos la tabla
        {
          this.setId(rs.getInt("id")); // => cursor
          this.setNombre(rs.getString("nombre")); // => cursor
          this.setCorreo(rs.getString("correo")); // => cursor
          this.setEdad(rs.getInt("edad")); // => cursor
        }
        
        if (this.getNombre() != "" && this.getNombre() != null){
            return true;
        } else {
            return false;
        }
        
    }
    
    @Override
    public String toString() {
        return "Estudiante{" + "id=" + id + ", nombre=" + nombre + ", correo=" + correo + ", edad=" + edad + '}';
    }
    
}
