/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */

// Variables globales que reprensentan nuestros Objetos Input.
// conts = constante
// let = sea x tal cosa / demostraciones, se usa en un contexto local
// var = global
// Document: objeto/instancia que representa el documento html
// Primero debemos esperar a que el documento html se cargue, para ello, usamos lo siguiente:
// addEventListener, es un método ó función del objeto document, con el cual
// recibe dos atributos, el primero es el tipo de evento.
// el segundo es el código javascript que se va a ejecutar cuando ocurra ese evento.

// en este caso DOMContentLoaded es el evento que indica cuando se cargó por completo el HTML.

function crearInputNumerico(x){
    var inputDinero = document.createElement("input");
    inputDinero.type = "number";
    inputDinero.id = "inputDinero";
    inputDinero.name = "inputDinero";
    

    x.appendChild(inputDinero);
}

document.addEventListener("DOMContentLoaded", function() { // Intancia que representa el contenido de la pagina
    var consultar = document.getElementById("consultar"); 
    var depositar = document.getElementById("depositar");
    var retirar = document.getElementById("retirar");
    var contenido = document.getElementById("contenido");
    var saldoActual = 0;
    
    
    consultar.onchange = function(){
            // imprimimos 'Hola mundo' en consola.
            contenido.innerHTML = "";
            var nuevoHTML = document.createElement("button");
            nuevoHTML.textContent = "Consultar";
            nuevoHTML.onclick = function(){
                let saldo = document.getElementById("saldo");
                saldo.textContent = saldoActual;
            };
            contenido.appendChild(nuevoHTML);
        };
        
    depositar.onchange = function(){
            contenido.innerHTML = "";
            
            crearInputNumerico(contenido);
            
            var btnDepositar = document.createElement("button");
            btnDepositar.textContent = "Depositar";
            btnDepositar.onclick = function(){
                saldoActual = saldoActual + parseFloat(inputDinero.value);
                let saldo = document.getElementById("saldo");
                saldo.textContent = saldoActual;
            };
            contenido.appendChild(btnDepositar);
        };
        
    retirar.onchange = function(){
            // imprimimos 'Hola mundo' en consola.
            contenido.innerHTML = "";
            
            crearInputNumerico(contenido);            
            
            var btnRetirar = document.createElement("button");
            btnRetirar.textContent = "Retirar";
            btnRetirar.onclick = function(){
                saldoActual = saldoActual - parseFloat(inputDinero.value);
                let saldo = document.getElementById("saldo");
                saldo.textContent = saldoActual;
            };
            contenido.appendChild(btnRetirar);
        };
    
});